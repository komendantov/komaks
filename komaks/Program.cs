﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace komaks
{
    class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }

   
  




    /// <summary>
    ///  Класс MyStem
    ///  работает с программой mystem
    ///  основная цель: вывод потока, состоящего из xml-кода, который вывела mystem
    /// </summary>
    public static class MyStemProgram
    {
        public static string filepath { get; set; }
        public static string tempPath { get; set; }
        static Process mystem = new Process();

        public static List<string> originalwords = new List<string>();
        public static List<string> replacedwords = new List<string>();

        public static string filereplace(string fileandpath, string enc)
        {
            Encoding encod;
            switch (enc)
            {
                case "cp1251":
                    encod = Encoding.Default;
                    break;
                case "utf8":
                    encod = Encoding.UTF8;
                    break;
                default:
                    encod = Encoding.Default;
                    break;
            }
            string text = File.ReadAllText(fileandpath, encod);
            string txt = replaceSlavon(text, MainForm.repl);
            var tempPath = Path.GetTempFileName();
            File.WriteAllText(tempPath, txt, encod);
            return tempPath;
        }


        public static string replaceSlavon(string str, bool en)
        {
            originalwords.Clear();
            replacedwords.Clear();
            string resultstr = str;
            switch (en)
            {
                case true:
                    resultstr = "";
                    string[] words = str.Split(' ');
                    foreach (string word in words)
                    {
                        string w = word;
                        originalwords.Add(w);
                        string r = w.Replace("ѯ", "кс").Replace("Ѯ", "Кс").Replace("ѡ", "о").Replace("ω", "о").Replace("Ѡ", "О").Replace("I", "И").Replace("i", "и");
                        //  string r = "Войсковоi".Replace("i", "и");
   
                        replacedwords.Add(r);
                    }
                    break;
                case false:
                    resultstr = str;
                    break;
            }
            foreach (string s in replacedwords)
            {
                resultstr = resultstr + s + " ";
            }
            return resultstr;
        }





        /// <summary>
        /// запуск программы mystem
        /// </summary>
        /// <returns>поток, состоящий из xml-кода, который вывела программа</returns>
        public static StreamReader myst(string encoding)
        {
            tempPath = filereplace(filepath, encoding);
            try
            {
                StreamReader reader;
                mystem.StartInfo.FileName = "mystem.exe";
                //   string antext = System.IO.File.ReadAllText(filepath);

                mystem.StartInfo.Arguments = "-e " + encoding + " -i -c -d -n --format xml " + "\"" + tempPath + "\"";
                mystem.StartInfo.UseShellExecute = false;
                mystem.StartInfo.CreateNoWindow = true;
                mystem.StartInfo.RedirectStandardOutput = true;
                mystem.Start();
                if (encoding == "cp1251")
                {
                    reader = new StreamReader(mystem.StandardOutput.BaseStream, Encoding.Default);
                }
                else { reader = new StreamReader(mystem.StandardOutput.BaseStream, Encoding.UTF8); }

              //  string strreader = reader.ReadToEnd();

                return reader;
            }
            catch
            {
                MessageBox.Show("Похоже на то, что не удаётся найти программу mystem. Или она неисправна. \nПоместите файл mystem.exe в одну директорию с исполняемым файлом этой программы. \nПрограмму mystem можно скачать отсюда http://tech.yandex.ru/mystem", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return null;
            }
        }

        /// <summary>
        /// закрытие программы mystem, вызывается отдельно
        /// </summary>
        public static void closemystem()
        {
            mystem.WaitForExit();
            System.IO.File.Delete(tempPath);
            mystem.Close();
        }
    }
}