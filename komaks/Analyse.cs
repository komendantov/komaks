﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace komaks
{

    /// <summary>
    /// Класс, содержащий в себе методы для анализа потока из программы mystem.
    /// В зависимости от требования выбирается тот или иной метод, возвращается результат
    /// </summary>
    public static class Analyse
    {
        public static string filename { get; set; }
        /// <summary>
        /// выбор файла - источника текста
        /// Присваивает полю  MyStemProgram.filepath значение, равное пути
        /// </summary>
        public static void selectfile()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog() { Filter = "Текстовые файлы(*.txt)|*.txt" };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)


                filename = openFileDialog1.FileName;
            if (filename != null)
            {
                MyStemProgram.filepath = filename;
            }
        }




        /// <summary>
        /// Метод, анализирующий поток.
        /// </summary>
        /// <returns>Строка, состоящая из слов, частей речи и разметки документа rtf</returns>
        public static string getpspeech(string encoding, List<string> selected, bool sadditional)
        {
            string[] obozn = { "вводн", "гео", "затр", "имя", "искаж", "мж", "обсц", "отч", "прдк", "разг", "редк", "сокр", "устар", "фам" };
            string result = null;
            string add = null;
            string replaced;
            string rpspeech = null;
            if ((filename != null))
            {
                StreamReader streamread = MyStemProgram.myst(encoding);
                if (streamread != null)
                {
                    try
                    {
                        XmlTextReader xmlreader = new XmlTextReader(streamread);
                        XDocument xDoc = XDocument.Load(xmlreader);
                        string[] grss;
                        //спускаемся вниз по xml
                        foreach (XElement gramems in xDoc.Element("html").Element("body").Elements("se"))//предложение
                        {
                           
                            foreach (XElement ana in gramems.Elements("w"))//слово
                            {
                                //   var hz = gramems.Nodes().OfType<XText>().FirstOrDefault().Value;
                             //   XText textNode = gramems.Nodes().OfType<XText>().FirstOrDefault();
                             //   XText textNode2 = gramems.Nodes().OfType<XText>().LastOrDefault();
                                foreach (XElement w in ana.Elements("ana"))
                                {
                                    if (MainForm.repl)
                                    {
                                        int index = MyStemProgram.replacedwords.IndexOf(ana.Value);
                                        if (MyStemProgram.replacedwords.Contains(ana.Value))
                                        {
                                          
                                            replaced = MyStemProgram.originalwords[index];
                                            MyStemProgram.replacedwords.RemoveAt(index);
                                            MyStemProgram.originalwords.RemoveAt(index);
                                        }
                                        else
                                            replaced = ana.Value;
                                    }
                                    else
                                        replaced = ana.Value;
                                    XAttribute lex = w.Attribute("lex");
                                    XAttribute gr = w.Attribute("gr");//граммемы (через запятую)
                                    grss = (Convert.ToString(gr.Value)).Split(',', '='); //заполнение граммемами массива
                                    Dictionary <string,string> rusGrammems = new Dictionary<string, string> {
                                        {"A", "прил." },
                                        {"ADV", "нареч." },
                                        {"ADVPRO","мест. нареч."  },
                                        {"ANUM", "числит.-прил." },
                                        {"APRO", "мест.-прил."},
                                        {"CONJ","с." },
                                        {"COM", "часть композита - слож. слова"},
                                        {"INTJ","междомет." },
                                        {"NUM","числит." },
                                        {"PART","частица" },
                                        { "PR", "предл."},
                                        {"S","сущ." },
                                        {"SPRO", "мест.-сущ."},
                                        {"V","гл." },

                                    };
                                    rpspeech = rusGrammems[grss[0]];
                                    if (grss[0] == "S")
                                    {
                                        if (sadditional == true)
                                        {
                                            try
                                            {
                                                if (obozn.Contains(grss[1]))
                                                {
                                                    add = "," + grss[1];
                                                }
                                            }
                                            catch { };
                                        }
                                    }
                                    if (grss[0] == "V")
                                    {
                                         try
                                                {
                                                    if (grss[6] == "прич")
                                                    {
                                                        rpspeech = "прич.";
                                                    }
                                                }
                                                catch
                                                {
                                                    try
                                                    {
                                                        if (grss[4] == "деепр")
                                                        {
                                                            rpspeech = "дееприч.";
                                                        }
                                                    }
                                                    catch { rpspeech = "гл."; }
                                                }
                                   
                                    }



                                    if (selected.Contains(rpspeech))//проверка, отмечена ли галочкой определенная часть речи
                                    {

                                        result = string.Concat(result,  replaced  + "(" + rpspeech + add + ")");
                                    }
                                    add = null;
                                    break;
                                }
                            }
                            if (selected.Count > 15)
                                result = string.Concat(result, ". ");

                        }
                        MyStemProgram.closemystem();
                    }
                    catch { MessageBox.Show("Ошибка при обработке файла программой. \nВозможно, вы выбрали неподдерживаемый файл или выбрали неправильную кодировку", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }

                return result;
            }
            else
            {
                MessageBox.Show("Ошибка, файл не был выбран", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }



        /// <summary>
        /// Метод, реализующий модуль "частотный анализ"
        /// </summary>
        /// <returns>Строка, состоящая из начальных форм слов, количества их форм и разметки документа rtf</returns>
        public static DataTable frequency(string encoding, bool sadditional)
        {

            int i = 0;
            int count = 1;
            DataTable table = new DataTable();
            table.Columns.Add("Слово", System.Type.GetType("System.String"));
            table.Columns.Add("Кол-во вхождений", System.Type.GetType("System.Int32"));
            string[] pos = { "A", "ADV", "COM", "NUM", "S", "V" };
            List<string> nfwords = new List<string>();//список под начальные формы
            List<string> nfwordsdis = new List<string>();//список под начальные формы, без повторов
            if ((filename != null))
            {

                StreamReader streamread = MyStemProgram.myst(encoding);
                if (streamread != null)
                {
                    /*     try
                         {*/
                    XmlTextReader xmlreader = new XmlTextReader(streamread);
                    XDocument xDoc = XDocument.Load(xmlreader);
                    string[] grss;
                    //спускаемся вниз по xml
                    foreach (XElement gramems in xDoc.Element("html").Element("body").Elements("se"))//предложение
                    {
                        foreach (XElement ana in gramems.Elements("w"))//слово
                        {
                            foreach (XElement w in ana.Elements("ana"))
                            {
                                XAttribute lex = w.Attribute("lex");
                                XAttribute gr = w.Attribute("gr");//граммемы (через запятую)
                                grss = (Convert.ToString(gr.Value)).Split(',', '='); //заполнение граммемами массива
                                if (sadditional == true)//если галочка отмечена то закидываем и служебные части речи
                                    nfwords.Add(lex.Value);
                                else//иначе только самостоятельные (перечислены в массиве pos)
                                {
                                    if (pos.Contains(grss[0]))
                                        nfwords.Add(lex.Value);
                                }
                                break;
                            }
                        }
                    }
                    MyStemProgram.closemystem();
                    /*    }
                     catch { MessageBox.Show("Ошибка при обработке файла программой. \nВозможно, вы выбрали неподдерживаемый файл или выбрали неправильную кодировку", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }*/
                    foreach (string word in nfwords)//пробегаем начальный массив
                    {
                        if (nfwordsdis.Contains(word) == false) //если конечный массив не содержит текущего слова
                        {
                            count = 1;//сбрасываем счетчик повторов для нового числа
                            foreach (string word1 in nfwords)
                                if (word == word1)
                                {
                                    if (nfwordsdis.Contains(word) == false) //если выбраного слова нет в конечном массиве добавить его
                                        nfwordsdis.Add(word);
                                    else
                                        count++;
                                }
                            table.Rows.Add(new object[]
{nfwordsdis[(nfwordsdis.Count - 1)], count});

                        }
                        else
                            continue;
                    }

                    foreach (string word in nfwords)
                    {
                        if (nfwordsdis.Contains(word) == false)
                        {
                            count = 1;
                            foreach (string word1 in nfwords)
                                if (word == word1)
                                {
                                    if (nfwordsdis.Contains(word) == false)
                                    { //если выбраного слова нет в конечном массиве добавить его
                                    }
                                }
                        }
                    }
                }

                return table;
            }
            else
            {
                MessageBox.Show("Ошибка, файл не был выбран", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }


        /// <summary>
        /// Метод, анализирующий поток.
        /// </summary>
        /// <returns>Строка, состоящая из слов, частей речи и разметки документа rtf</returns>
        public static string getimperative(string encoding)
        {

            string result = null;
            if ((filename != null))
            {

                StreamReader streamread = MyStemProgram.myst(encoding);
                if (streamread != null)
                {
                    try
                    {

                        XmlTextReader xmlreader = new XmlTextReader(streamread);
                        XDocument xDoc = XDocument.Load(xmlreader);
                        string[] grss;
                        bool pov = false;
                        //спускаемся вниз по xml
                        foreach (XElement gramems in xDoc.Element("html").Element("body").Elements("se"))//предложение
                        {
                            foreach (XElement ana in gramems.Elements("w"))//слово
                            {
                                foreach (XElement w in ana.Elements("ana"))
                                {
                                    XAttribute lex = w.Attribute("lex");
                                    XAttribute gr = w.Attribute("gr");//граммемы (через запятую)
                                    grss = (Convert.ToString(gr.Value)).Split(',', '='); //заполнение граммемами массива

                                    if (grss[0] == "V")
                                    {
                                        foreach (string el in grss)
                                        {

                                            if (el == "пов")
                                            {
                                                result = string.Concat(result, "(");
                                                pov = true;
                                            }
                                        }
                                    }

                                }
                                if (pov)
                                {
                                    result = string.Concat(result, ana.Value + ") ");
                                    pov = false;
                                }
                                else
                                    result = string.Concat(result, ana.Value + " ");



                            }

                            result = string.Concat(result, ". ");
                        }
                        MyStemProgram.closemystem();
                    }
                    catch { MessageBox.Show("Ошибка при обработке файла программой. \nВозможно, вы выбрали неподдерживаемый файл или выбрали неправильную кодировку", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
                return result;
            }
            else
            {
                MessageBox.Show("Ошибка, файл не был выбран", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }

}
