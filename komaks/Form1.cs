﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using FastColoredTextBoxNS;
using System.IO;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;



namespace komaks
{

    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            for (int i = 0; i < 16; i++)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
            
        }

        public string encoding;
        public static bool repl;
        
        public List<string> pofs = new List<string>();
        /// <summary>
        /// Части речи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            pathsOfSpeechWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(pathsOfSpeechWorker_RunWorkerCompleted);
            pathsOfSpeechWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Выбор файла и вывод пути к файлу вместе с именем в textBox1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Analyse.selectfile();
            textBox2.Text = Analyse.filename;
        }

        private void оПрограммеToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            AboutKomaks about = new AboutKomaks();
            about.Show();
        }

        private void сохранитьКакToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            RichTextBox temp = new RichTextBox();
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if (tabControl1.SelectedIndex == 0 && richTextBox1.Text != "")
            {
                saveFileDialog1.Filter = "RTF-формат|*.rtf|Текст|*.txt|HTML|*.html";
                saveFileDialog1.FileName = "Analyse";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    switch (saveFileDialog1.FilterIndex)
                    {
                        case 1:
                            temp.Text = richTextBox1.Text;
                            temp.Rtf = richTextBox1.Rtf;
                            temp.SaveFile(saveFileDialog1.FileName);
                            break;
                        case 2:
                            richTextBox1.SaveToFile(saveFileDialog1.FileName, System.Text.Encoding.UTF8);
                            break;
                        case 3:
                            string shtml = "<style>pre{white-space: pre-line;word-wrap:break-word;}</style>" + richTextBox1.Html;
                            File.WriteAllText(saveFileDialog1.FileName, shtml);
                            break;

                    }
                }
            }

            else if (tabControl1.SelectedIndex == 2 && fastColoredTextBox1.Text != "")
            { //проверка.
              //После дописания методов перенести ниже на 3 строки и добавить проверку открытой вкладки, дописав функцию сохранения из неё...if tabIndex=2 тада да...ы
                saveFileDialog1.Filter = "RTF-формат|*.rtf|Текст|*.txt|HTML|*.html";
                saveFileDialog1.FileName = "Analyse";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (saveFileDialog1.FilterIndex == 1)
                    {
                        temp.Text = fastColoredTextBox1.Text;
                        temp.Rtf = fastColoredTextBox1.Rtf;

                        temp.SaveFile(saveFileDialog1.FileName);
                    }
                    if (saveFileDialog1.FilterIndex == 3)
                    {
                        string html = "<!DOCTYPE html> <html>" + fastColoredTextBox1.Text;
                        int shift = "<!DOCTYPE html> <html>".Length;
                        string reg = @"\(.+?\)";
                        var rgs = fastColoredTextBox1.GetRanges(reg, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        foreach (FastColoredTextBoxNS.Range r in rgs)
                        {
                            html = html.Insert(r.Start.iChar + 1 + shift, "<SPAN style=\"color:#0000ff\">");
                            shift += "<SPAN style=\"color:#0000ff\">".Length;
                            html = html.Insert(r.End.iChar - 1 + shift, "</SPAN>");
                            shift += "</SPAN>".Length;
                        }
                        html += "</html>";
                        File.WriteAllText(saveFileDialog1.FileName, html);
                    }
                    if (saveFileDialog1.FilterIndex == 2)
                        // richTextBox1.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                        fastColoredTextBox1.SaveToFile(saveFileDialog1.FileName, System.Text.Encoding.UTF8);
                }
            }
            //Сохранение из модуля частотного анализа

            else if (tabControl1.SelectedIndex == 1 && dataGridView1.Columns.Count > 0)//проверка выбранного модуля "Частотный анализ" индексы с нуля!
            {
                if (dataGridView1.RowCount != 0)//проверка не пуста ли таблица(проводился ли анализ)
                {
                    // SaveFileDialog saveFileDialog1 = new SaveFileDialog();//диалоговое окно сохранеиня файла
                    saveFileDialog1.Filter = "XLSX - таблица Excel |*.xlsx|Текст|*.txt|HTML|*.html";
                    saveFileDialog1.FileName = "Частотный анализ вывод";
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if (saveFileDialog1.FilterIndex == 1)//если выбрано сохранение в .xlsx
                        {
                            Excel.Application excelapp = new Excel.Application();
                            Excel.Workbook workbook = excelapp.Workbooks.Add();
                            Excel.Worksheet worksheet = workbook.ActiveSheet;
                            excelapp.Columns.ColumnWidth = 17;
                            worksheet.Cells[1, 1] = dataGridView1.Columns[0].HeaderText;
                            worksheet.Cells[1, 2] = dataGridView1.Columns[1].HeaderText;
                            for (int i = 1; i < dataGridView1.RowCount + 1; i++)//строки
                            {
                                for (int j = 1; j < dataGridView1.ColumnCount + 1; j++)//столбцы
                                {
                                    worksheet.Rows[i + 1].Columns[j] = dataGridView1.Rows[i - 1].Cells[j - 1].Value;
                                }
                            }
                            excelapp.AlertBeforeOverwriting = false;
                            saveFileDialog1.OverwritePrompt = false;
                            workbook.SaveAs(saveFileDialog1.FileName);
                            excelapp.Quit();
                        }
                        if (saveFileDialog1.FilterIndex == 2)//если выбрано сохранение в .txt
                        {
                            StringBuilder builder = new StringBuilder();
                            int rowcount = dataGridView1.Rows.Count;//число строк
                            int columncount = 2;//число столбцов
                            List<string> headerCols = new List<string>();//пишем названия колонок
                            for (int j = 0; j < columncount; j++)
                            {
                                headerCols.Add(dataGridView1.Columns[j].HeaderText);
                            }
                            builder.Append(string.Join(" (", headerCols));
                            builder.AppendLine(")");
                            for (int i = 0; i < rowcount - 1; i++)//пробегаем строки
                            {
                                List<string> cols = new List<string>();
                                for (int j = 0; j < columncount; j++)//пробегаем колонки
                                {
                                    cols.Add(dataGridView1.Rows[i].Cells[j].Value.ToString());
                                }
                                builder.Append(string.Join(" (", cols.ToArray()));
                                builder.AppendLine(")");
                            }
                            System.IO.File.WriteAllText(saveFileDialog1.FileName, builder.ToString());
                        }
                        if (saveFileDialog1.FilterIndex == 3)//если выбрано сохранение в .html
                        {
                            StringBuilder strB = new StringBuilder();
                            strB.AppendLine("<html><body><center><" +
                            "table border='1' cellpadding='0' cellspacing='0'>");
                            strB.AppendLine("<tr>");
                            for (int i = 0; i < dataGridView1.Columns.Count; i++)
                            {
                                strB.AppendLine("<td align='center' valign='middle'>" +
                                dataGridView1.Columns[i].HeaderText + "</td>");
                            }
                            strB.AppendLine("<tr>");
                            for (int i = 0; i < dataGridView1.Rows.Count; i++)
                            {
                                strB.AppendLine("<tr>");
                                foreach (DataGridViewCell dgvc in dataGridView1.Rows[i].Cells)
                                {
                                    strB.AppendLine("<td align='center' valign='middle'>" +
                                    dgvc.Value.ToString() + "</td>");
                                }
                                strB.AppendLine("</tr>");
                            }
                            strB.AppendLine("</table></center></body></html>");
                            System.IO.File.WriteAllText(saveFileDialog1.FileName, strB.ToString());
                            MessageBox.Show("Вкладка 2, таблица не пуста, сохранение в html");
                        }
                    }
                }
            }
            else { MessageBox.Show("Ошибка! Нечего сохранять. \nПожалуйста, проведите анализ.", "Ошибка1!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }


        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
            fastColoredTextBox1.Copy();
        }

        private void копироватьВсёToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabControl1.TabIndex == 0)
                richTextBox1.SelectAll();
            richTextBox1.Copy();
            if (tabControl1.TabIndex == 1)
                richTextBox1.SelectAll();
            richTextBox1.Copy();
            if (tabControl1.TabIndex == 2)
                richTextBox1.SelectAll();
            richTextBox1.Copy();
        }

        private void PathsOfSpeechWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            repl = checkBox3.Checked;
            Invoke(new MethodInvoker(() => button1.Enabled = false));
            Invoke(new MethodInvoker(() => encoding = comboBox1.Text));
            Invoke(new MethodInvoker(() => setPofs()));
            string toform = Analyse.getpspeech(encoding, pofs, checkBox1.Checked);
            Invoke(new MethodInvoker(() => richTextBox1.Text = toform));
            string reg = @"\(.+?\)";
            var rgs = richTextBox1.GetRanges(reg, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            foreach (FastColoredTextBoxNS.Range r in rgs)
            {
                r.SetStyle(BlueStyle);
            }
        }
        Style BlueStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
        Style GreenStyle = new TextStyle(Brushes.Green, null, FontStyle.Italic);
        public void setPofs()
        {
            pofs.Clear();
            var sel = checkedListBox1.CheckedItems;
            foreach (string index in sel)
                pofs.Add(index);
        }
        private void pathsOfSpeechWorker_RunWorkerCompleted(
        object sender,
        RunWorkerCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            Invoke(new MethodInvoker(() => button1.Enabled = true));
        }

        //Вкладка "Частотный анализ"------------------------------------------------------------------------------------------------------------------------------------
        private void button3_Click(object sender, EventArgs e)//Кнопка Анализ
        {
            progressBar1.Visible = true;

            backgroundWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorker2_RunWorkerCompleted);
            backgroundWorker2.RunWorkerAsync();
            DataTable table = new DataTable();
            table.Columns.Add("Слово", System.Type.GetType("System.String"));
            table.Columns.Add("Кол-во вхождений", System.Type.GetType("System.Int32"));
        }


        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            Invoke(new MethodInvoker(() => encoding = comboBox1.Text));
            DataTable toformf = Analyse.frequency(encoding, checkBox2.Checked);
            Invoke(new MethodInvoker(() => dataGridView1.DataSource = toformf));
            Invoke(new MethodInvoker(() => button3.Enabled = false)); //активация кнопки Анализ
        }
        private void backgroundWorker2_RunWorkerCompleted(
        object sender,
        RunWorkerCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            Invoke(new MethodInvoker(() => button3.Enabled = true));
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e) { }// Галочка показа второстепенных членов
        private void richTextBox2_TextChanged(object sender, EventArgs e) { }
        private void tabPage2_Click(object sender, EventArgs e) { }

        //побуждения
        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            Invoke(new MethodInvoker(() => button4.Enabled = false));
            Invoke(new MethodInvoker(() => encoding = comboBox1.Text));
            string toform = Analyse.getimperative(encoding);
            Invoke(new MethodInvoker(() => fastColoredTextBox1.Text = toform));
            string reg = @"\(.+?\)";
            var rgs = fastColoredTextBox1.GetRanges(reg, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            foreach (FastColoredTextBoxNS.Range r in rgs)
            {
                r.SetStyle(BlueStyle);
            }
        }
        private void backgroundWorker3_RunWorkerCompleted(
        object sender,
        RunWorkerCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            Invoke(new MethodInvoker(() => button4.Enabled = true));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            backgroundWorker3.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorker3_RunWorkerCompleted);
            backgroundWorker3.RunWorkerAsync();
        }
    }
}

